<?php 

class Peso{

	private $id;
	private $usuario_id;
	private $peso;
	private $grasa;
	private $grasav;
	private $musculo;
	private $calorias;
	private $pliegue;
	private $cintura;
	private $fecha;

	private $db;

	public function __construct() {
		$this->db = Database::connect();
	}

	function getId(){
		return $this->id;
	}
	function getUsuarioId(){
		return $this->usuario_id;
	}
	function getPeso(){
		return $this->peso;
	}
	function getGrasa(){
		return $this->grasa;
	}
	function getGrasav(){
		return $this->grasav;
	}
	function getMusculo(){
		return $this->musculo;
	}
	function getCalorias(){
		return $this->calorias;
	}
	function getPliegue(){
		return $this->pliegue;
	}
	function getCintura(){
		return $this->cintura;
	}
	function getFecha(){
		return $this->fecha;
	}
	function setId($id){
		$this->id=$id;
	}
	function setUsuarioId($usuario_id){
		$this->usuario_id=$usuario_id;
	}
	function setPeso($peso){
		$this->peso=$this->db->real_escape_string($peso);
	}
	function setGrasa($grasa){
		$this->grasa=$this->db->real_escape_string($grasa);
	}
	function setGrasav($grasav){
		$this->grasav=$this->db->real_escape_string($grasav);
	}
	function setMusculo($musculo){
		$this->musculo=$this->db->real_escape_string($musculo);
	}
	function setCalorias($calorias){
		$this->calorias=$this->db->real_escape_string($calorias);
	}
	function setPliegue($pliegue){
		$this->pliegue=$this->db->real_escape_string($pliegue);
	}
	function setCintura($cintura){
		$this->cintura=$this->db->real_escape_string($cintura);
	}
	function setFecha($fecha){
		$this->fecha=$this->db->real_escape_string($fecha);
	}
	public function getAll(){
		$usuario_id=intval($this->getUsuarioId());
		$pesos= $this->db->query("SELECT * FROM peso WHERE usuario_id=$usuario_id ORDER BY id DESC;");
		return $pesos;
	}
	public function getNombreUsuario(){
		$usuario_id=intval($this->getUsuarioId());
		$nombre= $this->db->query("SELECT Nombre FROM usuarios WHERE id=$usuario_id");
		$cadena=mysqli_fetch_object($nombre);
		return $cadena->Nombre;
	}

	public function save(){
		$usuario=intval($this->getUsuarioId());
		$pes=floatval($this->getPeso());
		$gras=floatval($this->getGrasa());
		$grasv=intval($this->getGrasav());
		$musc=floatval($this->getMusculo());
		$cal=intval($this->getCalorias());
		$plieg=floatval($this->getPliegue());
		$cint=intval($this->getCintura());
		


		$sql = "INSERT INTO peso VALUES (NULL, $usuario, $pes, $gras, $grasv, $musc, $cal, $plieg, $cint, CURDATE(), CURTIME())";
		$save= $this->db->query($sql);
		$result=false;
		if ($save) {
			$result=true;
		}
		return $result;
	}
	public function delete(){
		$sql="DELETE FROM peso WHERE id={$this->id}";
		$delete=$this->db->query($sql);
		$result=false;
		if ($delete) {
			$result=true;
		}
		return $result;
	}

	


}
 ?>