<div id="crear" >
	<h1>Crear registro de peso</h1>
	<form action="<?=base_url?>peso/guardar" method="POST">
		<label for="peso">Peso (kg)</label>
		<input type="number" step="0.01" name="peso" required/>
		<label for="grasa">% grasa</label>
		<input type="number" step="0.01" name="grasa" required />
		<label for="grasav">% grasa visceral</label>
		<input type="number" step="1" name="grasav" required />
		<label for="musculo">% musculo</label>
		<input type="number" step="0.01" name="musculo" required />
		<label for="calorias">Calorias (kcal)</label>
		<input type="number" step="1" name="calorias" required />
		<label for="pliegue">Pliegue(mm)</label>
		<input type="number" step="0.1" name="pliegue" required />
		<label for="cintura">Cintura (cm)</label>
		<input type="number" step="1" name="cintura" required />
		<input type="submit" value="Guardar" />
	</form>
</div>