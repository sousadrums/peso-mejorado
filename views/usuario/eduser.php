<div  class="formulario">
	<h1>Editar datos de usuario</h1>
	<form action="<?=base_url?>usuario/update" method="POST">
		<label for="nombre">Nombre</label>
		<input type="text" name="Nombre" value="<?=isset($nombre)  ? $nombre : '';?>" required /><br>
		<label for="apellidos" >Apellidos</label>
		<input type="text" name="Apellidos" value="<?=isset($apellidos)  ? $apellidos : '';?>"required /><br>
		<label for="email">email</label>
		<input type="email" name="correo" value="<?=isset($email)  ? $email : '';?>"required /><br>
		<label for="nacimiento">Fecha de nacimiento</label>
		<input type="date" name="nacimiento" value="<?=isset($nacimiento)  ? $nacimiento : '';?>"required /><br>
		<label for="talla">Talla (cm)</label>
		<input type="number" name="talla" value="<?=isset($talla)  ? $talla : '';?>"required /><br>
		<input type="submit" value="Guardar" />
	</form>
	<br>
	<a href="<?=base_url?>usuario/contraseña" class="button">Cambiar contraseña</a>
</div>