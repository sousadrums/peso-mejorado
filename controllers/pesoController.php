<?php 

require_once 'models/peso.php';

class pesoController{

	function introducir(){
		require_once 'views/peso/introducir.php';
		if (isset($_SESSION['admin'])) {
				require_once 'views/usuario/admin.php';	
			}elseif(isset($_SESSION['nutri'])){
				require_once 'views/usuario/supervisor.php';
			}else{
				require_once 'views/usuario/user.php';
			}

	}
	function guardar(){
		if (isset($_POST)) {
			$peso=new Peso();
			$peso->setUsuarioid($_SESSION['identity']->id);
			$peso->setPeso($_POST['peso']);
			$peso->setGrasa($_POST['grasa']);
			$peso->setGrasav($_POST['grasav']);
			$peso->setMusculo($_POST['musculo']);
			$peso->setCalorias($_POST['calorias']);
			$peso->setPliegue($_POST['pliegue']);
			$peso->setCintura($_POST['cintura']);
			$save=$peso->save();
			if ($save) {
				$_SESSION['tema']="complete";
			}else{
				$_SESSION['tema']="failed";
			}
		}else{
			$_SESSION['tema']="failed";
		}
		header("Location:".base_url."peso/ver");
	}
	function ver(){
		$peso=new Peso();
		if (isset($_GET['id'])) {
			$peso->setUsuarioid($_GET['id']);
		}else{
			$peso->setUsuarioId($_SESSION['identity']->id);
		}
		$nombre=$peso->getNombreUsuario();
		$registros=$peso->getAll();
		require_once 'views/peso/ver.php';
		if (isset($_SESSION['admin'])) {
				require_once 'views/usuario/admin.php';	
			}elseif(isset($_SESSION['nutri'])){
				require_once 'views/usuario/supervisor.php';
			}else{
				require_once 'views/usuario/user.php';
			}
	}
	function delete(){
		if (isset($_GET['id'])) {
			$id=$_GET['id'];
			$peso= new Peso();
			$peso->setId($id);
			$delete=$peso->delete();
			if ($delete) {
				$_SESSION['delete']='complete';
			}else{
				$_SESSION['delete']='failed';
			}

		}else{
			$_SESSION['delete']='failed';
		}

		header("Location:".base_url."peso/ver");


	}



}


 ?>