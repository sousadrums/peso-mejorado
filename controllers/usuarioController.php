<?php
require_once 'models/usuario.php';

class usuarioController{

	public function index(){
		require_once 'views/usuario/login.php';
		
	}



	public function register(){
		require_once 'views/usuario/registro.php';
	}

	public function loginV(){
		require_once 'views/usuario/login.php';
	}
	public function loged(){

		if (isset($_SESSION['admin'])){
			 require_once 'views/usuario/admin.php';
		}elseif(isset($_SESSION['nutri'])){
			require_once 'views/usuario/supervisor.php';
		}elseif(isset($_SESSION['identity'])){
				require_once 'views/usuario/user.php';
		}else{
				require_once 'views/usuario/login.php';
				echo '<h4>Usuario y/o contraseña incorrectos. Introduce de nuevo tus credenciales.</h4>';
		}
	}

	public function usuarios(){
		Utils::isAdmin();
		$usuario=new Usuario();
		$usuarios=$usuario->getAll();
		if (isset($_SESSION['admin'])){
			 require_once 'views/usuario/admin.php';
		}
		require_once 'views/usuario/usuarios.php';
	}
	public function usuariosNut(){
		Utils::isNutri();
		$usuario=new Usuario();
		$usuarios=$usuario->getAll();
		if (isset($_SESSION['nutri'])){
			 require_once 'views/usuario/supervisor.php';
		}
		require_once 'views/usuario/usuarios.php';
	}


	public function login(){
		if(isset($_POST)){
			// Identificar al usuario
			// Consulta a la base de datos
			$usuario = new Usuario();
			$usuario->setEmail($_POST['email']);
			$usuario->setPassword($_POST['password']);
			$identity = $usuario->login();
			


			if($identity && is_object($identity)){
				$_SESSION['identity'] = $identity;
				
				if($identity->rol == 'admin'){
					$_SESSION['admin'] = true;
				}elseif($identity->rol == 'nutri'){
					$_SESSION['nutri'] = true;
				}
				
			}else{
				$_SESSION['error_login'] = 'Identificación fallida !!';
			}
		
		}
		header("Location:".base_url."usuario/loged");
	}
	
	public function logout(){
		if(isset($_SESSION['identity'])){
			unset($_SESSION['identity']);
		}
		
		if(isset($_SESSION['admin'])){
			unset($_SESSION['admin']);
		}
		if(isset($_SESSION['nutri'])){
			unset($_SESSION['nutri']);
		}
		
		header("Location:".base_url."usuario/loginV");
	}

	public function save(){
		if (isset($_POST)) {
			
			$nombre = isset($_POST['nombre']) ? $_POST['nombre'] : false;
			$apellidos = isset($_POST['apellidos']) ? $_POST['apellidos'] : false;
			$email = isset($_POST['email']) ? $_POST['email'] : false;
			$password = isset($_POST['password']) ? $_POST['password'] : false;
			$nacimiento= isset($_POST['nacimiento']) ? $_POST['nacimiento'] : false;
			$talla= isset($_POST['talla']) ? $_POST['talla'] : false;

			
			if($nombre && $apellidos && $email && $password && $nacimiento && $talla){
				$usuario= new Usuario();
				$usuario->setNombre($nombre);
				$usuario->setApellidos($apellidos);
				$usuario->setEmail($email);
				$usuario->setPassword($password);
				$usuario->setNacimiento($nacimiento);
				$usuario->setTalla($talla);
				
				$save= $usuario->save();

				if ($save) {
					$_SESSION['register']= "complete" ;
				}else{
					$_SESSION['register']= "failed";
				}
			}else{
				$_SESSION['register'] = "failed";
			}

		}else{
			$_SESSION['register']= "Failed";
		}
		header("Location:".base_url.'usuario/register');


	}

	public function edit(){
		if (isset($_SESSION)) {
			$nombre=$_SESSION['identity']->Nombre;
			$apellidos=$_SESSION['identity']->Apellidos;
			$email=$_SESSION['identity']->correo;
			$nacimiento=$_SESSION['identity']->nacimiento;
			$talla=$_SESSION['identity']->talla;
	
			if (isset($_SESSION['admin'])) {
				require_once 'views/usuario/admin.php';	
			}elseif(isset($_SESSION['nutri'])){
				require_once 'views/usuario/supervisor.php';
			}else{
				require_once 'views/usuario/user.php';
			}
			require_once 'views/usuario/eduser.php';

		}
	}


	public function delete(){
		Utils::isAdmin();
			if (isset($_GET['id'])) {
				$id=$_GET['id'];
				$usuario= new Usuario();
				$usuario->setId($id);
				$delete=$usuario->delete();
				if ($delete) {
					$_SESSION['delete']='complete';
				}else{
					$_SESSION['delete']='failed';
				}

			}else{
				$_SESSION['delete']='failed';
			}


		header("Location:".base_url."usuario/usuarios");
	}

	public function update(){
		if (isset($_POST)) {
			$usuario = new Usuario();
			$usuario->setId($_SESSION['identity']->id);
			$usuario->setNombre($_POST['Nombre']);
			$usuario->setApellidos($_POST['Apellidos']);
			$usuario->setEmail($_POST['correo']);
			$usuario->setNacimiento($_POST['nacimiento']);
			$usuario->setTalla($_POST['talla']);
			$save=$usuario->edit();
			var_dump($_SESSION['identity']);

			session_unset();


			if ($save) {
				$_SESSION['usuario']="complete";
			}else{
				$_SESSION['usuario']="failed";
			}

		}else{
			$_SESSION['usuario']="failed";
		}
		header("Location:".base_url);
	}

	public function contraseña(){
		if (isset($_SESSION['admin'])) {
				require_once 'views/usuario/admin.php';
			}else{
				require_once 'views/usuario/user.php';
			}
		require_once 'views/usuario/ccontraseña.php';
	}
	public function ccontraseña(){
		if (isset($_POST)) {
			$old=$_POST['old'];
			$new1=$_POST['new1'];
			$new2=$_POST['new2'];
			$verify1 = password_verify($old, $_SESSION['identity']->password);

			$verify2=false;
			if ($new1==$new2) {
				$verify2=true;
			}
			if (!$verify1) {
				echo '<h1>La contraseña no es correcta</h1>';
			}elseif (!$verify2) {
				echo "<h1>Las contraseñas no coinciden</h1>";
			}else{
				$usuario = new Usuario();
				$usuario->setId($_SESSION['identity']->id);
				$usuario->setPassword($new2);
				$save=$usuario->updatePassword();
				session_unset();
				if ($save) {
					$_SESSION['uPassword']="complete";
					
				}else{
					$_SESSION['uPassword']="failed";
			}
			}
		
		}
		header("Location:".base_url);
		
	}

}

